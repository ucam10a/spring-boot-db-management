package com.yung.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.yung.ApplicationContextManager;
import com.yung.StartWebApplication;
import com.yung.tool.ClobConverter;
import com.yung.tool.IpAddressUtils;
import com.yung.tool.NumberConverter;
import com.yung.tool.TimeConverter;
import com.yung.tool.TraceTool;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

/**
 * Use Tomcat 8 above
 * 
 * 
 * @author Yung-Long Li
 *
 */
@Controller
public class MainController {

    private static final Logger log = LoggerFactory.getLogger(MainController.class);
    private static final TraceTool traceTool = TraceTool.getTraceTool(log, true);
    
    private static final ClobConverter cConverter = new ClobConverter();
    private static final NumberConverter nConverter = new NumberConverter();
    private static final TimeConverter tConverter = new TimeConverter();
    
    @Autowired
    private HttpServletRequest request;
    
    private String getRequestIpAddress() {
        String ipAddress = request.getRemoteAddr();
        if (IpAddressUtils.isPrivateIP(ipAddress)) {
            String remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr != null || !"".equals(remoteAddr)) {
                ipAddress = request.getRemoteAddr();
            }
        }
        return ipAddress;
    }
    
    @GetMapping("/")
    public String main(Model model) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        
        String color = ApplicationContextManager.getServletContext().getInitParameter("backgroundColor");
        log.info("color: " + color);
        
        String ipAddress = getRequestIpAddress();
        traceTool.reflectTrace("main", "ipAddress", ipAddress);
        model.addAttribute("ipAddress", ipAddress);
        //view
        return "welcome";
    }

    @GetMapping("/sqlServlet")
    public String sqlMain(Model model) {

        //view
        return "index";
        
    }
    
    @PostMapping("/sqlServlet")
    public String runSql(HttpServletRequest req, Model model) throws ServletException {

        String sql = req.getParameter("sql");
        if (sql == null) {
            sql = "";
        }
        req.setAttribute("sql", sql);
        log.info("sql: " + sql);
        
        int rowCnt = 0;
        boolean moreRecord = false;
        StringBuilder headerSb = new StringBuilder();
        StringBuilder rowsSb = new StringBuilder();
        if ("".equals(sql)) {
            headerSb.append("<tr><td></td></tr>");
            rowsSb.append("<tr><td></td></tr>");
        } else {
            PreparedStatement ps = null;
            ResultSet rs = null;
            Connection conn = getConnection();
            try {
                ps = conn.prepareStatement(sql);
                String lowerSql = sql.toLowerCase();
                lowerSql = lowerSql.trim();
                if (lowerSql.startsWith("select")) {
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        if (rowCnt > 1000) {
                            moreRecord = true;
                            break;
                        }
                        ResultSetMetaData metaData = rs.getMetaData();
                        int cnt = metaData.getColumnCount();
                        if (headerSb.toString().equals("")) {
                            if (cnt > 0) {
                                headerSb.append("<tr>");
                            }
                            for (int i = 1; i <= cnt; i++) {
                                String columnName = metaData.getColumnName(i);
                                headerSb.append("<td>");
                                headerSb.append(columnName);
                                headerSb.append("</td>");
                            }
                            if (cnt > 0) {
                                headerSb.append("</tr>");
                            }
                        }
                        if (cnt > 0) {
                            rowsSb.append("<tr>");
                        }
                        for (int i = 1; i <= cnt; i++) {
                            Object val = rs.getObject(i);
                            String content = convertVal(val);
                            rowsSb.append("<td>");
                            rowsSb.append(content);
                            rowsSb.append("</td>");
                        }
                        if (cnt > 0) {
                            rowsSb.append("</tr>");
                        }
                        rowCnt++;
                    }
                    if (headerSb.toString().equals("")) {
                        headerSb.append("<tr><td></td></tr>");
                        rowsSb.append("<tr><td></td></tr>");
                    }
                } else {
                    ps.executeUpdate();
                }
                req.setAttribute("headerTitles", headerSb.toString());
                req.setAttribute("rows", rowsSb.toString());
                req.setAttribute("rowCnt", rowCnt + "");
                if (moreRecord) {
                    req.setAttribute("sqlMessage", "success! but only show first 1000 records!");
                } else {
                    req.setAttribute("sqlMessage", "success!");
                }
                
                
            } catch (Exception e) {
                log.error(e.toString(), e);
                throw new ServletException(e);
            } finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    throw new ServletException(e);
                }
            }
        }
        
        //view
        return "index";
        
    }
    
    private Connection getConnection() {
        String jndiPath = StartWebApplication.getProperty("jndi.path");
        String driver = StartWebApplication.getProperty("jdbc.driver");
        String user = StartWebApplication.getProperty("jdbc.user");
        String pass = StartWebApplication.getProperty("jdbc.pass");
        String url = StartWebApplication.getProperty("jdbc.url");
        if (jndiPath != null && !"".equals(jndiPath)) {
            return getConnection(jndiPath);
        } else if (driver != null && !"".equals(driver)) {
            return getConnection(driver, user, pass, url);
        }
        log.warn("No Jdbc setting!");
        return null;
    }

    public static Connection getConnection(String jndi) {
        try {
            InitialContext ctx = new InitialContext();
            return  ((DataSource) ctx.lookup(jndi)).getConnection();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConnection(String driver, String user, String password, String url) {
        try {
            try {
                Class.forName(driver);
            } catch (Exception e) {
                log.error(e.toString(), e);
                throw new RuntimeException(e);
            }
            Connection conn = DriverManager.getConnection(url, user, password);
            return conn;
        } catch (SQLException e) {
            log.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    private String convertVal(Object val) throws Exception {
        if (val == null) {
            return "";
        }
        if (val instanceof String) {
            return val.toString();
        }
        if (val instanceof Clob) {
            String content = (String) cConverter.Convert(val, String.class);
            return content;
        }
        if (nConverter.isNumber(val.getClass())) {
            BigDecimal num = (BigDecimal) nConverter.Convert(val, BigDecimal.class);
            return num.toPlainString();
        }
        if (tConverter.isTime(val.getClass())) {
            Date dt = (Date) tConverter.Convert(val, Date.class);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(dt);
        }
        return "";
    }
    
}