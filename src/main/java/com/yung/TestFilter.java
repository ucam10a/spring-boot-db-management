package com.yung;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yung.tool.TraceTool;

@WebFilter(asyncSupported=true,urlPatterns="/*")
public class TestFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(TestFilter.class);
    private static final TraceTool traceTool = TraceTool.getTraceTool(logger, false);
    
    public void destroy() {
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
        traceTool.trace("TestFilter ...");
        
        // Pass control on to the next filter
        chain.doFilter(request, response);
    
    }

}
