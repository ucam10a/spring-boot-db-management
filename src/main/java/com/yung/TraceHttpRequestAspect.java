package com.yung;

import java.lang.reflect.Parameter;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.yung.tool.TraceTool;

@Aspect
@Component
public class TraceHttpRequestAspect {

    private static final Logger log = LoggerFactory.getLogger(TraceHttpRequestAspect.class);
    private static final TraceTool traceTool = TraceTool.getTraceTool(log, false);
    private static final int KEEP_RUN_SECONDS = 0;

    @Pointcut("execution(public * com.yung.controller.*.*(..))")
    public void executeService() {
    }

    @Around("executeService()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        
        // Before Method
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();

        String url = request.getRequestURL().toString();
        String method = request.getMethod();
        String uri = request.getRequestURI();
        String queryString = request.getQueryString();
        traceTool.reflectTrace("doAround", "url", url);
        traceTool.reflectTrace("doAround", "method", method);
        traceTool.reflectTrace("doAround", "uri", uri);
        traceTool.reflectTrace("doAround", "queryString", queryString);
        
        String className = pjp.getTarget().getClass().getName();
        traceTool.reflectTrace("doAround", "className", className);
        String methodName = pjp.getSignature().getName();
        traceTool.reflectTrace("doAround", "methodName", methodName);
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Parameter[] parameters = signature.getMethod().getParameters();
        Object[] args = pjp.getArgs();
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                traceTool.reflectTrace("doAround", "args[" + i + "] type", parameters[i].getParameterizedType().getTypeName());
                traceTool.reflectTrace("doAround", "args[" + i + "] name", parameters[i].getName());
                if (args[i] != null) {
                    traceTool.reflectTrace("doAround", "args[" + i + "] value", args[i].toString());
                } else {
                    traceTool.reflectTrace("doAround", "args[" + i + "]", "null");
                }
            }
        }
        
        long start = System.currentTimeMillis();
        
        // result is return value
        Object result = pjp.proceed();
        
        long lapse = System.currentTimeMillis() - start;
        if (lapse < (KEEP_RUN_SECONDS * 1000)) {
            // always keep job minimum run time KEEP_RUN_SECONDS seconds
            traceTool.reflectTrace("doAround", "sleep", "sleep: " + ((KEEP_RUN_SECONDS * 1000) - lapse) + " mills");
            Thread.sleep((KEEP_RUN_SECONDS * 1000) - lapse);
        }
        
        // After method
        traceTool.reflectTrace("doAround", "result", result);
        
        return result;
        
    }
    
    @Before("executeService()")
    public void doBefore(JoinPoint joinPoint) {
        traceTool.trace("doBefore");
    }

    @After("executeService()")
    public void doAfter(JoinPoint joinPoint) {
        traceTool.trace("doAfter");
    }

    @AfterReturning("executeService()")
    public void doAfterReturning(JoinPoint joinPoint) {
        traceTool.trace("doAfterReturning");
    }

    @AfterThrowing("executeService()")
    public void deAfterThrowing(JoinPoint joinPoint) {
        traceTool.trace("deAfterThrowing");
    }
    
}
