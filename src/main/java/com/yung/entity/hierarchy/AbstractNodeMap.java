package com.yung.entity.hierarchy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import com.yung.tool.JavaLogDebugPrinter;

/**
 * abstract execute tool and node map<br>
 * implement this abstract class will need to defined whole node hierarchy
 * 
 * @author Yung-Long Li
 *
 */
public abstract class AbstractNodeMap {

    private boolean LOCK = false;
    
    private List<String> log = new ArrayList<String>();
    
    private final Map<String, DependencyNode> globalNodeMap = new HashMap<String, DependencyNode>();
    
    public AbstractNodeMap() {
        createHierarchy();
    }
    
    /**
     * create and define whole node hierarchy
     * 
     */
    public abstract void createHierarchy();
    
    /**
     * build a node
     * 
     * @param entity entity
     * @return node
     */
    public DependencyNode buildNode(Class<?> entity) {
        if (globalNodeMap.containsKey(entity.getName())) {
            return globalNodeMap.get(entity.getName());
        }
        DependencyNode node = new DependencyNode(entity, this);
        globalNodeMap.put(entity.getName(), node);
        return node;
    }
    
    /**
     * build a node
     * 
     * @param entity entity
     * @param methodName fire method
     * @return node
     */
    public DependencyNode buildNode(Class<?> entity, String methodName) {
        DependencyNode node = buildNode(entity);
        Method method = findMethod(methodName);
        node.setMethod(method);
        return node;
    }
    
    private Method findMethod(String methodName) {
        Method[] methods = this.getClass().getDeclaredMethods();
        for (Method method : methods) {
            if (method.getName().equalsIgnoreCase(methodName)) {
                return method;
            }
        }
        throw new RuntimeException(methodName + " is not found!");
    }

    public List<String> getLog() {
        return log;
    }
    
    /**
     * execute function
     * 
     * @param args arguments
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    public List<String> execute(Object... args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (LOCK == true) {
            throw new RuntimeException(this.getClass().getSimpleName() + " is running, please wait");
        }
        try {
            JavaLogDebugPrinter printer = new JavaLogDebugPrinter();
            printer.addFilterType(this.getClass().getName());
            LOCK = true;
            log.clear();
            TreeSet<DependencyNode> nodes = new TreeSet<DependencyNode>();
            nodes.addAll(globalNodeMap.values());
            String hierarchy = printer.getPrintObjectMessage("node hierarchy", nodes);
            log.add("-----------------------------\n" + hierarchy);
            Map<String, Message> executeMap = new ConcurrentHashMap<String, Message>();
            for (DependencyNode node : nodes) {
                executeMap.put(node.getName(), new Message());
            }
            log.add("******execute steps");
            for (DependencyNode node : nodes) {
                if (executeMap.get(node.getName()).isAllowFire() == false) {
                    log.add(this.getClass().getSimpleName() + " execute " + node.getName() + " not fired, because of " + executeMap.get(node.getName()).getErrormsg() + ".");
                    continue;
                }
                boolean success = node.fireMethod(args);
                log.add(this.getClass().getSimpleName() + " execute " + node.getName() + " fired, success: " + success);
                if (success == false) {
                    Set<DependencyNode> set = new HashSet<DependencyNode>();
                    node.getAllSubNode(set);
                    for (DependencyNode subNode : set) {
                        Message msg = new Message();
                        msg.setAllowFire(false);
                        msg.setErrormsg(node.getName() + " execute fail");
                        executeMap.put(subNode.getName(), msg);
                    }
                }
            }
            log.add("-----------------------------");
            List<String> ret = new ArrayList<String>();
            ret.addAll(log);
            return ret;
        } finally {
            LOCK = false;
            log.clear();
        }
    }
    
}
