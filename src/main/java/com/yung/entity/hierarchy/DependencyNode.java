package com.yung.entity.hierarchy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Basic node for dependency structure<br>
 * It defines entity hierarchy, dependency and execute method
 * 
 * @author Yung-Long Li
 *
 */
public class DependencyNode implements Comparable<DependencyNode> {
    
    /** entity */
    private Class<?> entity;
    
    /** entity name */
    private String name;
    
    /** entity hierarchy level, 0 means the highest top entity */
    private int level = 0;
    
    /** method to execute */
    private Method method;
    protected String methodName;
    
    private AbstractNodeMap runInstance;
    
    private Set<DependencyNode> subNodeSet = new LinkedHashSet<DependencyNode>();
    
    /**
     * constructor
     * 
     * @param entity entity
     * @param runInstance execute object
     */
    protected DependencyNode(Class<?> entity, AbstractNodeMap runInstance) {
        // private constructor
        this.entity = entity;
        this.name = entity.getName();
        this.runInstance = runInstance;
    }
    
    public int getLevel() {
        return level;
    }
    
    private void setLevel(int level) {
        this.level = level;
        for (DependencyNode subNode : subNodeSet) {
            int subNodeLevel = subNode.getLevel();
            if (subNodeLevel < (this.level + 1)) {
                subNode.setLevel(this.level + 1);
            }
        }
    }
    
    public Set<DependencyNode> getSubNodeSet() {
        return subNodeSet;
    }

    public void setSubNodeSet(Set<DependencyNode> subNodeSet) {
        this.subNodeSet = subNodeSet;
    }
    
    public void addSubNode(DependencyNode subNode) {
        subNodeSet.add(subNode);
        int subNodeLevel = subNode.getLevel();
        if (subNodeLevel < (this.level + 1)) {
            subNode.setLevel(this.level + 1);
        }
    }
    
    public int hashCode() {
        return 17 * entity.getName().hashCode();
    }

    public String getName() {
        return name;
    }
    
    public boolean hasSubNode() {
        if (subNodeSet.size() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public void getAllSubNode(Set<DependencyNode> set) {
        for (DependencyNode node : subNodeSet) {
            set.add(node);
            node.getAllSubNode(set);
        }
    }
    
    public void setMethod(Method method) {
        this.method = method;
        if (method != null) {
            this.methodName = method.getName();
        }
    }
    
    /**
     * fire method
     * 
     * @param args arguments
     * @return true for success or fail for fail
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    public boolean fireMethod(Object... args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (method == null) {
            runInstance.getLog().add(name + " does not define method, then skip.");
            return true;
        }
        Object ret = null;
        try {
            ret = method.invoke(runInstance, args);
        } catch (Exception e) {
            runInstance.getLog().add("method: " + method.getName() + ", " + e.toString());
            ret = false;
        }
        if (ret instanceof Boolean) {
            return (Boolean) ret;
        }
        throw new RuntimeException(method.getName() + " return type is not boolean!");
    }

    @Override
    public int compareTo(DependencyNode o) {
        if (o.getLevel() == this.getLevel()) {
            if (o.getName().equals(this.getName()) ) {
                return 0;
            } else if (o.getName().hashCode() > this.getName().hashCode()) {
                return -1;
            } else {
                return 1;
            }
        } else if (o.getLevel() > this.getLevel()) {
            return -1;
        } else {
            return 1;
        }
    }

}
