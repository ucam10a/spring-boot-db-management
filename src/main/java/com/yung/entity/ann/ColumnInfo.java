package com.yung.entity.ann;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The custom annotation for POJOReprotBean field
 * 
 * @author Yung Long Li
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
// on field level
public @interface ColumnInfo {

    /** excel column name */
    String name() default "";

    /** excel column description */
    String dateFormat() default "";

}