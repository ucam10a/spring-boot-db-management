package com.yung;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Component;

/**
 * For test only
 * 
 * 
 * @author Yung-Long Li
 *
 */
@Component
public class SpringExecuteEngine implements CommandLineRunner {
    
    public static void main(String[] args) {
        SpringApplication.run(SpringExecuteEngine.class, args);
    }

    public void run(String... args) throws Exception {
        
        //List<String> log = tool.execute();
        //for (String msg : log) {
        //    System.out.println(msg);
        //}
        
        // System exist
        //int exitCode = SpringApplication.exit(ApplicationContextManager.getContext(), new ExitCodeGenerator() {
        //    @Override
        //    public int getExitCode() {
        //        // no errors
        //        return 0;
        //    }
        //});
        //System.exit(exitCode);
        
    }

}
