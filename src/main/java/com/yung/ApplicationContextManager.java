package com.yung;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

public class ApplicationContextManager {

    private static final Logger log = LoggerFactory.getLogger(ApplicationContextManager.class);
    
    private static ApplicationContext context;
    
    private static ServletContext servletContext;

    public static ApplicationContext getContext() {
        if (context == null) {
            log.warn("context is null, not setup!");
        }
        return context;
    }
    
    public static ServletContext getServletContext() {
        if (servletContext == null) {
            log.warn("servletContext is null, not setup!");
        }
        return servletContext;
    }

    public static void setContext(ApplicationContext context) {
        if (context == null) {
            log.warn("context is null, not setup!");
        }
        ApplicationContextManager.context = context;
    }
    
    public static void setServletContext(ServletContext servletContext) {
        if (servletContext == null) {
            log.warn("context is null, not setup!");
        }
        ApplicationContextManager.servletContext = servletContext;
    }
    
}
