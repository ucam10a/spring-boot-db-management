package com.yung.tool;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.yung.entity.Schema;

public class SchemaCheckTool {

	private static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
	
	public static List<String> validate(Map<String, Schema> schemaMap, Object obj) throws Exception {
		List<String> errorList = new ArrayList<String>();
		Object keyId = null;
		for (Schema sch : schemaMap.values()) {
			Object val = null;
			String embedIdName = sch.getEmbedIdField();
			if (embedIdName != null && !"".equals(embedIdName)) {
				if (keyId == null) {
					keyId = PlainObjectOperator.runGetter(obj, embedIdName);
				}
				val = PlainObjectOperator.runGetter(keyId, sch.getName());
			} else {
				val = PlainObjectOperator.runGetter(obj, sch.getName());
			}
			String error = null;
			if ("string".equals(sch.getType())) {
				error = validateString((String) val, sch);
			} else if ("number".equals(sch.getType())) {
				error = validateNumber(val, sch);
			} else if ("date".equals(sch.getType())) {
				error = validateDate(val, sch);
			}
			if (error != null) {
				errorList.add(error);
			}
		}
		return errorList;
	}
	
	public static String validateString(String src, Schema sch) {
		if (sch.isAllowNull() != true) {
			if (src == null) {
				return "[" + sch.getName() + "(string)] is null, but not allowed.";
			} else {
				int length = sch.getLength();
				if (calLength(src) > length) {
					return "[" + sch.getName() + "(string)]'" + src + "' length is over " + length;
				}
			}
		}
		return null;
	}
	
	/**
     * check string contains only ASCII
     * 
     * @param checkString
     * @return boolean is ASCII
     */
    public static boolean isASCII(String checkString) {
        if (checkString == null || "".equals(checkString.trim())) {
            return true;
        }
        return asciiEncoder.canEncode(checkString);
    }
	
	public static int calLength(String input) {
		if (input == null) {
			return 0;
		}
		int length = 0;
		for (int i = 0; i < input.length(); i++) {
			String c = input.charAt(i) + "";
			if (isASCII(c)) {
				length++;
			} else {
				length = length + 3;
			}
		}
		return length;
	}
	
	public static String validateDate(Object val, Schema sch) {
		if (sch.isAllowNull() != true && val == null) {
			return "[" + sch.getName() + "(date)] is null, but not allowed.";
		}
		return null;
	}

	public static String validateNumber(Object val, Schema sch) {
		if (sch.isAllowNull() != true && val == null) {
			return "[" + sch.getName() + "(number)] is null, but not allowed.";
		}
		return null;
	}
	
}