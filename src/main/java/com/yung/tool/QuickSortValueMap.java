package com.yung.tool;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * The Map which is sorted by the value
 */
public class QuickSortValueMap<K, V extends Comparable<V>> implements Map<K, V> {
    
    public QuickSortValueMap(){
    }
    
    public QuickSortValueMap(boolean reverseOrder){
        if (reverseOrder) {
            this.equal = new TreeMap<V, ArrayList<K>>(Collections.reverseOrder());
        }
    }
    
    private class SimpleEntry implements Map.Entry<K, V> {
        private K key;
        private V value;
        private SimpleEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
        @Override
        public K getKey() {
            return key;
        }
        @Override
        public V getValue() {
            return value;
        }
        @Override
        public V setValue(V value) {
            this.value = value;
            return value;
        }
    }
    
    /**
     * the source data map
     */
    public HashMap<K, V> base = new HashMap<K, V>();

    /**
     * store equal value
     */
    public TreeMap<V, ArrayList<K>> equal = new TreeMap<V, ArrayList<K>>();

    public void clear() {
        base.clear();
        equal.clear();
    }

    public boolean containsKey(Object key) {
        return base.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return base.containsValue(value);
    }

    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entries = new LinkedHashSet<Entry<K, V>>();
        for (K k : keySet()) {
            Entry<K, V> entry = new SimpleEntry(k, get(k));
            entries.add(entry);
        }
        return entries;
    }

    @Override
    public Set<K> keySet() {
        LinkedHashSet<K> keySet = new LinkedHashSet<K>();
        for (java.util.Map.Entry<V, ArrayList<K>> entry : equal.entrySet()) {
            ArrayList<K> keyList = entry.getValue();
            for (K key : keyList) {
                keySet.add(key);
            }
        }
        return keySet;
    }

    public V get(Object key) {
        return base.get(key);
    }

    public boolean isEmpty() {
        return base.isEmpty();
    }

    public V put(K key, V value) {
        removeOldKey(key, value);
        if (equal.get(value) == null) {
            ArrayList<K> keyList = new ArrayList<K>();
            keyList.add(key);
            equal.put(value, keyList);
        } else {
            equal.get(value).add(key);
        }
        return base.put(key, value);
    }

    private void removeOldKey (K key, V value) {
        V oldValue = base.get(key);
        if (oldValue == null) {
            return;
        } else {
            ArrayList<K> keyList = equal.get(oldValue);
            keyList.remove(key);
        }
    }
    
    public void putAll(Map<? extends K, ? extends V> m) {
        for (K key : m.keySet()) {
            put(key, m.get(key));
        }
    }

    public V remove(Object key) {
        V value = base.get(key);
        ArrayList<K> keyList = equal.get(value);
        keyList.remove(key);
        return base.remove(key);
    }

    public int size() {
        return base.size();
    }

    public Collection<V> values() {
        Collection<V> values = new ArrayList<V>();
        for (java.util.Map.Entry<V, ArrayList<K>> entry : equal.entrySet()) {
            ArrayList<K> keyList = entry.getValue();
            for (int i = 0; i < keyList.size(); i++) {
                values.add(entry.getKey());
            }
        }
        return values;
    }

    public static void main(String args[]) {

        QuickSortValueMap<String, Integer> score = new QuickSortValueMap<String, Integer>();

        score.put("b", 3);
        score.put("a", 4);
        score.put("c", 1);
        score.put("e", 6);
        score.put("a", 7);
        score.put("b", 3);
        score.put("d", 3);
        score.put("e", 7);

        System.out.println("size = " + score.size());

        for (Object key : score.keySet()) {
            System.out.println("key = " + key + ", value = " + score.get(key));
        }

        QuickSortValueMap<String, String> test = new QuickSortValueMap<String, String>();

        test.put("a", "dsfsd");
        test.put("b", "sdag");
        test.put("c", "asg");
        test.put("e", "ccc");
        test.put("d", "hj");
        test.put("f", "ccc");
        test.put("g", "ccc");
        test.put("e", "e5yh");
        test.put("e", "sdjj");

        System.out.println("size = " + test.size());

        for (Object key : test.keySet()) {
            System.out.println("key = " + key + ", value = " + test.get(key));
        }
        for (String val : test.values()) {
            System.out.println(val);
        }
        for (Entry<String, String> entry : test.entrySet()){
            System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
        }
        
        System.out.println("==== remove key");
        test.remove("f");
        for (Object key : test.keySet()) {
            System.out.println("key = " + key + ", value = " + test.get(key));
        }
        for (String val : test.values()) {
            System.out.println(val);
        }
        for (Entry<String, String> entry : test.entrySet()){
            System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
        }
        
    }

}