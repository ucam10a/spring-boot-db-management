package com.yung.tool;

import java.io.IOException;

public interface ZipStringTool {

    public String compress(String srcTxt) throws IOException;

    public String decompress(String zippedBase64Str) throws IOException;
    
}
