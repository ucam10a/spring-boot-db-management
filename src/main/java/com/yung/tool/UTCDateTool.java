package com.yung.tool;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UTCDateTool {

	private static final Logger log = LoggerFactory.getLogger(UTCDateTool.class);
	private static final boolean throwException = false;
	
	public final static String format = "yyyy-MM-dd#HH:mm:ss.SSS "; 
	
	public static void logError(String entityInfo, String error) {
		log.error("***UTCDateTool ERROR*** " + entityInfo + error);
	}
	
	private static boolean validate(String input, String... entityInfo) {
		if (input.length() != "yyyy-MM-ddTHH:mm:ss.SSSZ".length() || !input.endsWith("Z")) {
			String error = "Time format[" + input + "] is not yyyy-MM-ddTHH:mm:ss.SSSZ";
			if (throwException) {
				throw new RuntimeException(error);
			} else {
				if (entityInfo != null && entityInfo[0] != null) {
					log.error("***UTCDateTool ERROR*** " + entityInfo[0] + " " + error);
				}
				return false;
			}
		}
		return true;
	}
	
	public static Object parseString(String input) {
		if (input.length() != "yyyy-MM-ddTHH:mm:ss.SSSZ".length() || !input.endsWith("Z")) {
			String error = "Time format[" + input + "] is not yyyy-MM-ddTHH:mm:ss.SSSZ";
			return error;
		}
		input = input.replace('T', '#');
		input = input.replace('Z', ' ');
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(input);
		} catch (ParseException e) {
			return e.getMessage();
		}
	}
	
	private static java.util.Date parse(String input, String... entityInfo) {
		input = input.replace('T', '#');
		input = input.replace('Z', ' ');
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(input);
		} catch (ParseException e) {
			if (throwException) {
				throw new RuntimeException(e);
			} else {
				if (entityInfo != null && entityInfo[0] != null) {
					log.error("***UTCDateTool ERROR*** " + entityInfo[0] + " " + e.getMessage());
				}
				return null;
			}
		}
	}
	
	public static Timestamp convertTsp(String input, String... entityInfo) {
		if (input == null) {
			return null;
		}
		boolean valid = validate(input, entityInfo);
		if (valid == false) {
			return null;
		}
		java.util.Date ret = parse(input, entityInfo);
		if (ret == null) {
			return null;
		}
		return new Timestamp(parse(input).getTime());
	}
	
	public static java.sql.Date convertDate(String input, String... entityInfo) {
		if (input == null) {
			return null;
		}
		boolean valid = validate(input, entityInfo);
		if (valid == false) {
			return null;
		}
		String dateString = input.substring(0, "yyyy-MM-dd".length());
		return java.sql.Date.valueOf(dateString);
	}
	
	public static java.util.Date convertTime(String input, String... entityInfo) {
		if (input == null) {
			return null;
		}
		boolean valid = validate(input, entityInfo);
		if (valid == false) {
			return null;
		}
		java.util.Date ret = parse(input, entityInfo);
		if (ret == null) {
			return null;
		}
		return ret;
	}
	
}