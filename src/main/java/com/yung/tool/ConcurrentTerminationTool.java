package com.yung.tool;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ConcurrentTerminationTool {

    private static void awaitTerminationAfterShutdown(ExecutorService threadPool, int waitSeconds) {
        threadPool.shutdown();
        try {
            if (!threadPool.awaitTermination(waitSeconds, TimeUnit.SECONDS)) {
                threadPool.shutdownNow();
            }
        } catch (InterruptedException ex) {
            threadPool.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
    
    public static void executeAndWaitTermination(ExecutorService threadPool, CopyOnWriteArrayList<Runnable> taskList, int waitSeconds) {
        for (Runnable task : taskList) {
            threadPool.execute(task);    
        }
        awaitTerminationAfterShutdown(threadPool, waitSeconds);
    }
    
    public static void executeAndWaitTermination(int poolSize, CopyOnWriteArrayList<Runnable> taskList, int waitSeconds) {
        ExecutorService threadPool = Executors.newScheduledThreadPool(poolSize);
        for (Runnable task : taskList) {
            threadPool.execute(task);    
        }
        awaitTerminationAfterShutdown(threadPool, waitSeconds);
    }
    
}