package com.yung.tool;

import java.util.regex.Pattern;

/**
 * @author jittagornp <http://jittagornp.me>
 * create : 2017/10/31
 */
public class IpAddressUtils {

    private IpAddressUtils() {

    }

    private static boolean isValidFormat(String text) {
        Pattern pattern = Pattern.compile("^[\\d]+\\.[\\d]+\\.[\\d]+\\.[\\d]+$");
        return pattern.matcher(text).matches();
    }

    private static boolean isClassA(short[] ipNumber) {
        return ipNumber[0] == 10;
    }

    private static boolean isClassB(short[] ipNumber) {
        return ipNumber[0] == 172
                && (ipNumber[1] >= 16 && ipNumber[1] <= 31);
    }

    private static boolean isClassC(short[] ipNumber) {
        return ipNumber[0] == 192 && ipNumber[1] == 168;
    }

    public static boolean isPrivateIP(String ipAddress) {
        if (!isValidFormat(ipAddress)) {
            return false;
        }
        String[] ip = ipAddress.split("\\.");
        short[] ipNumber = new short[]{
            Short.parseShort(ip[0]),
            Short.parseShort(ip[1]),
            Short.parseShort(ip[2]),
            Short.parseShort(ip[3])
        };

        return isClassA(ipNumber) || isClassB(ipNumber) || isClassC(ipNumber);
    }

}
