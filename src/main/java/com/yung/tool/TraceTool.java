package com.yung.tool;

import java.net.URL;
import java.util.List;

/**
 * Convenient trace tool to trace log easily 
 * GPL License
 * 
 * @author Yung Long Li
 *
 */
public class TraceTool {
    
    /** Debug flag should turn off in production environment */
    protected static boolean DEBUG_ENABLE = true;
    
    /** Debug method start prefix */
    protected static final String DEBUG_START_PREFIX = "----------------------------------------------------------";
    
    /** Debug method end prefix */
    protected static final String DEBUG_END_PREFIX = "==========================================================";
    
    /** Debug log prefix */
    protected static final String DEBUG_PREFIX = "**********************************************************";
    
    /** Server logger */
    private ServerDebugPrinter printer;
    
    /** trace flag */
    private boolean flag = false;
    
    /**
     * Constructor
     * 
     * @param cls trace class
     * @param log server logger
     */
    public TraceTool(AppLogger log) {
        this.printer = new ServerDebugPrinter();
        this.printer.setServerLogger(log);
        if (DEBUG_ENABLE == false) {
            printer.warnMessage(DEBUG_PREFIX + "DEBUG_ENABLE is false!");
        } else {
            printer.warnMessage(DEBUG_PREFIX + "trace turn on !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            this.flag = true;
        }
    }
    public TraceTool(org.slf4j.Logger log) {
        this.printer = new ServerDebugPrinter();
        this.printer.setServerLogger(log);
        if (DEBUG_ENABLE == false) {
            printer.warnMessage(DEBUG_PREFIX + "DEBUG_ENABLE is false!");
        } else {
            printer.warnMessage(DEBUG_PREFIX + "trace turn on !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            this.flag = true;
        }
    }
    
    public TraceTool(AppLogger log, boolean flag) {
        this.printer = new ServerDebugPrinter();
        this.printer.setServerLogger(log);
        if (DEBUG_ENABLE == false) {
            if (flag == true) {
                printer.warnMessage(DEBUG_PREFIX + "DEBUG_ENABLE is false!");
            }
        } else {
            if (flag == true) {
                printer.warnMessage(DEBUG_PREFIX + " trace turn on !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                this.flag = flag;
            }
        }
    }
    public TraceTool(org.slf4j.Logger log, boolean flag) {
        this.printer = new ServerDebugPrinter();
        this.printer.setServerLogger(log);
        if (DEBUG_ENABLE == false) {
            if (flag == true) {
                printer.warnMessage(DEBUG_PREFIX + "DEBUG_ENABLE is false!");
            }
        } else {
            if (flag == true) {
                printer.warnMessage(DEBUG_PREFIX + " trace turn on !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                this.flag = flag;
            }
        }
    }
    
    public static TraceTool getTraceTool(AppLogger log) {
        return new TraceTool(log, true);
    }
    public static TraceTool getTraceTool(org.slf4j.Logger log) {
        return new TraceTool(log, true);
    }
    
    public static TraceTool getTraceTool(AppLogger log, boolean flag) {
        return new TraceTool(log, flag);
    }
    public static TraceTool getTraceTool(org.slf4j.Logger log, boolean flag) {
        return new TraceTool(log, flag);
    }
    
    /**
     * 
     * 
     * @param start
     * @param method
     */
    public void trace(boolean start, String method) {
        if (flag) {
            if (start) {
                printer.printMessage(DEBUG_START_PREFIX + "[" + method + "] start ... ");    
           } else {
                printer.printMessage(DEBUG_END_PREFIX + "[" + method + "] end");
           }
        }
    }
    
    /**
     * trace list object <br> 
     * print object to string
     * 
     * @param args list object
     */
    public void trace(List<Object> args) {
        trace(args.toArray());
    }
    
    /**
     * trace object array <br>
     * print object to string
     * 
     * @param args object array
     */
    public void trace(Object... args) {
        if (flag) {
            StringBuffer sf = new StringBuffer();
            sf.append("\n");
            for (int i = 0; i < args.length; i++) {
                sf.append(args[i].toString());
                sf.append("\n");
            }
            printer.printMessage(DEBUG_PREFIX + "remark message:" + sf.toString() + DEBUG_PREFIX);
        }
    }
    
    /**
     * trace object <br>
     * print object name and value
     * 
     * @param names object name array
     * @param args object array
     */
    public void trace(String[] names, Object[] args) {
        if (flag) {
            StringBuffer sf = new StringBuffer();
            sf.append("\n");
            for (int i = 0; i < names.length; i++) {
                sf.append(names[i] + ": " + args[i].toString());
                sf.append("\n");
            }
            printer.printMessage(DEBUG_PREFIX + "[" + sf.toString() + "]\n" + DEBUG_PREFIX);
        }
    }
    
    /**
     * reflect to call object and print object
     * 
     * @param method method name
     * @param names object name array
     * @param args object array
     */
    public void reflectTrace(String method, String[] names, Object[] args) {
        if (flag) {
            printer.printMessage(DEBUG_PREFIX + "[" + method + "]");
            for (int i = 0; i < names.length; i++) {
                printer.printObjectParam(names[i], args[i]);
            }
            printer.printMessage(DEBUG_PREFIX);
        }
    }
    
    /**
     * reflect to call object and print object
     * 
     * @param method method name
     * @param name object name
     * @param obj object
     */
    public void reflectTrace(String method, String name, Object obj) {
        reflectTrace(method, new String[]{name}, new Object[]{obj});
    }
    
    /**
     * trace jar file location
     * 
     * @param cls class
     */
    public void traceClassJarFile(Class<?> cls) {
        if (flag != true) {
            return;
        }
        ClassLoader  classloader = cls.getClassLoader();
        String name = cls.getName();
        name = replaceAll(name, ".", "/");
        URL res = classloader.getResource(name + ".class");
        String path = res.getPath();
        printer.printObjectParam("class[" + cls.getSimpleName() + "] jar from", path);
    }
    
    private static String replaceAll(String originalText, String key, String replaceValue){
        if (originalText == null || originalText.length() == 0 || key == null || key.length() == 0 ||
                replaceValue == null) return originalText;
        int fromIndex = 0, idx, len = key.length();
        StringBuffer sb = new StringBuffer();
        boolean found = false;
        while ((idx = originalText.indexOf(key, fromIndex)) != -1) {
            if (!found) found = true;
            sb.append(originalText.substring(fromIndex, idx));
            sb.append(replaceValue);
            fromIndex = idx + len;
        }
        if (!found) return originalText;
        if (fromIndex < originalText.length()) {
            sb.append(originalText.substring(fromIndex));
        }
        return sb.toString();
    }
    
}