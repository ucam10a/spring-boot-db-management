package com.yung.tool;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DecimalTool {

	private static final Logger log = LoggerFactory.getLogger(UTCDateTool.class);
	private static final boolean throwException = false;
	
	private static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
	
	public static void logError(String entityInfo, String error) {
		log.error("***DecimalTool ERROR*** " + entityInfo + error);
	}
	
	private static boolean validate(String input, String... entityInfo) {
		if (!isNumeric(input)) {
			String error = "Number format[" + input + "] is not number";
			if (throwException) {
				throw new RuntimeException(error);
			} else {
				if (entityInfo != null && entityInfo[0] != null) {
					log.error("***DecimalTool ERROR*** " + entityInfo[0] + " " + error);
				}
				return false;
			}
		}
		return true;
	}
	
	private static BigDecimal parse(String input, String... entityInfo) {
		try {
			return new BigDecimal(input);
		} catch (Exception e) {
			if (throwException) {
				throw new RuntimeException(e);
			} else {
				if (entityInfo != null && entityInfo[0] != null) {
					log.error("***DecimalTool ERROR*** " + entityInfo[0] + " " + e.getMessage());
				}
				return null;
			}
		}
	}
	
	public static BigDecimal convertDecimal(String input, String... entityInfo) {
		if (input == null || "".equals(input)) {
			return null;
		}
		boolean valid = validate(input, entityInfo);
		if (valid == false) {
			return null;
		}
		BigDecimal ret = parse(input, entityInfo);
		if (ret == null) {
			return null;
		}
		return ret;
	}
	
}