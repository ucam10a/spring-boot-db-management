function stringifyJSON(o) {
	
	if (o === null) {
		return null;
	}

	var pairs, k, name, val, type = jQuery.type(o);

	if (type === 'undefined') {
		return undefined;
	}

	// Also covers instantiated Number and Boolean objects,
	// which are typeof 'object' but thanks to jQuery.type, we
	// catch them here. I don't know whether it is right
	// or wrong that instantiated primitives are not
	// exported to JSON as an {"object":..}.
	// We choose this path because that's what the browsers did.
	if (type === 'number' || type === 'boolean') {
		return String(o);
	}
	if (type === 'string') {
		return quoteString(o);
	}
	/*
	if (typeof o.toJSON === 'function') {
		return jQuery.toJSON(o.toJSON());
	}
	 */
	if (type === 'date') {
		var month = o.getUTCMonth() + 1, day = o.getUTCDate(), year = o
				.getUTCFullYear(), hours = o.getUTCHours(), minutes = o
				.getUTCMinutes(), seconds = o.getUTCSeconds(), milli = o
				.getUTCMilliseconds();

		if (month < 10) {
			month = '0' + month;
		}
		if (day < 10) {
			day = '0' + day;
		}
		if (hours < 10) {
			hours = '0' + hours;
		}
		if (minutes < 10) {
			minutes = '0' + minutes;
		}
		if (seconds < 10) {
			seconds = '0' + seconds;
		}
		if (milli < 100) {
			milli = '0' + milli;
		}
		if (milli < 10) {
			milli = '0' + milli;
		}
		return '"' + year + '-' + month + '-' + day + 'T' + hours + ':'
				+ minutes + ':' + seconds + '.' + milli + 'Z"';
	}

	pairs = [];

	if (jQuery.isArray(o)) {
		for (k = 0; k < o.length; k++) {
			pairs.push(stringifyJSON(o[k]) || 'null');
		}
		return '[' + pairs.join(',') + ']';
	}

	// Any other object (plain object, RegExp, ..)
	// Need to do typeof instead of jQuery.type, because we also
	// want to catch non-plain objects.
	if (typeof o === 'object') {
		for (k in o) {
			// Only include own properties,
			// Filter out inherited prototypes
			if (typeof hasOwn == 'undefined' || hasOwn.call(o, k)) {
				// Keys must be numerical or string. Skip others
				type = typeof k;
				if (type === 'number') {
					name = '"' + k + '"';
				} else if (type === 'string') {
					name = quoteString(k);
				} else {
					continue;
				}
				type = typeof o[k];

				// Invalid values like these return undefined
				// from toJSON, however those object members
				// shouldn't be included in the JSON string at all.
				if (type !== 'function' && type !== 'undefined') {
					val = stringifyJSON(o[k]);
					pairs.push(name + ':' + val);
				}
			}
		}
		return '{' + pairs.join(',') + '}';
	}
}

function myReplaceAll (targetStr, strFind, strReplace) {
    var index = 0;
    while (targetStr.indexOf(strFind, index) != -1) {
        targetStr = targetStr.replace(strFind, strReplace);
        index = targetStr.indexOf(strFind, index);
    }
    return targetStr;
}

function quoteString (str) {
    if (str.match(escape)) {
        return '"' + str.replace(escape, function (a) {
            var c = meta[a];
            if (typeof c === 'string') {
                return c;
            }
            c = a.charCodeAt();
            return '\\u00' + Math.floor(c / 16).toString(16) + (c % 16).toString(16);
        }) + '"';
    }
    str = myReplaceAll(str, '"', '\'');
    return '"' + str + '"';
}

//define JSON for old IE version
if (window["JSON"] == null) {
    window["JSON"] = {};
    window["JSON"]["stringify"] = function (obj) {
        return stringifyJSON(obj);
    }
    
    // JSON RegExp
    var rvalidchars = /^[\],:{}\s]*$/;
    var rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g;
    var rvalidescape = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g;
    var rvalidtokens = /"[^"\\\r\n]*"|true|false|null|-?(?:\d\d*\.|)\d+(?:[eE][\-+]?\d+|)/g;
    
    window["JSON"]["parse"] = function (data) {
        if ( !data || typeof data !== "string") {
            return null;
        }

        // Make sure leading/trailing whitespace is removed (IE can't handle it)
        data = jQuery.trim( data );

        // Make sure the incoming data is actual JSON
        // Logic borrowed from http://json.org/json2.js
        if ( rvalidchars.test( data.replace( rvalidescape, "@" )
            .replace( rvalidtokens, "]" )
            .replace( rvalidbraces, "")) ) {

            return ( new Function( "return " + data ) )();

        }
        jQuery.error( "Invalid JSON: " + data );
    }
}