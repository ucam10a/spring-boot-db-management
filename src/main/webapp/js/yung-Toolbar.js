/**
 * To defined tool bar item schema
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ToolbarItem = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.ToolbarItem"
    },
    
    /**
     * item image path
     * @member {string}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemImg : null,
    
    /**
     * item label text
     * @member {string}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemLabel : null,
    
    /**
     * item tooltip text
     * @member {string}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemTooltip : "",
    
    /**
     * item function to run
     * @member {function}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemFunct : null,
    
    
    /**
     * constructor
     * @memberof com_yung_util_ToolbarItem
     * @param {string} itemImg - item image path
     * @param {string} itemLabel - item label text
     * @param {string} itemTooltip - item tooltip text
     * @param {createTabCallback} itemFunct - item function to run
     */
    init: function(itemImg, itemLabel, itemTooltip, itemFunct){
        if (itemImg == null && itemLabel == null) {
            throw "please specified itemImg or itemLabel";
        }
        this.setProperty("itemImg", itemImg);
        this.setProperty("itemLabel", itemLabel);
        this.setProperty("itemTooltip", itemTooltip);
        this.setProperty("itemFunct", itemFunct);
        return this;
    },
    
    /** 
     * set property value
     * 
     * @instance
     * @memberof com_yung_util_ToolbarItem
     * @param {string} attribute - property key
     * @param {allType} value - property value
     */
    setProperty : function (attribute, value) {
        if (_validType('string', attribute) == false) {
            throw "attribute is not string";
        }
        var ch = attribute.charAt(0) + "";
        var method = "set" + ch.toUpperCase() + attribute.substring(1);
        if (typeof this[method] == 'function') {
            this[method](value);
        }
    },
    
    /** 
     * get property value
     * 
     * @instance
     * @memberof com_yung_util_ToolbarItem
     * @param {string} attribute - property key
     * @return {allType} property value
     */
    getProperty : function (attribute) {
        if (_validType('string', attribute) == false) {
            throw "attribute is not string";
        }
        var ch = attribute.charAt(0) + "";
        var method = "get" + ch.toUpperCase() + attribute.substring(1);
        if (typeof this[method] == 'function') {
            return this[method]();
        }
        return null;
    },
    setItemImg : function (itemImg) {
        if (itemImg != null && _validType('string', itemImg) == false) {
            throw "itemImg is not string";
        }
        this.itemImg = itemImg;
    },
    getItemImg : function () {
        return this.itemImg;
    },
    setItemLabel : function (itemLabel) {
        if (itemLabel != null && _validType('string', itemLabel) == false) {
            throw "itemLabel is not string";
        }
        this.itemLabel = itemLabel;
    },
    getItemLabel : function () {
        return this.itemLabel;
    },
    setItemTooltip : function (itemTooltip) {
        if (itemTooltip != null && _validType('string', itemTooltip) == false) {
            throw "itemTooltip is not string";
        }
        this.itemTooltip = itemTooltip;
    },
    getItemTooltip : function () {
        return this.itemTooltip;
    },
    setItemFunct : function (itemFunct) {
        if (itemFunct != null && _validType('function', itemFunct) == false) {
            throw "itemFunct is not function";
        }
        this.itemFunct = itemFunct;
    },
    getItemFunct : function () {
        return this.itemFunct;
    }

});

/**
 * Create toolbar tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Toolbar = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.Toolbar" 
    },
    
    /**
     * toolbar container
     * @member {HTMLElement}
     * @instance
     * @memberof com_yung_util_Toolbar
     */
    container : null,
    
    /**
     * toolbar button css
     * @member {CSSStyle}
     * @instance
     * @memberof com_yung_util_Toolbar
     */
    btnCSS : null,
    
    /**
     * toolbar background color
     * @member {string}
     * @instance
     * @memberof com_yung_util_Toolbar
     */
    color : "rgb(234, 234, 234)",
    
    /**
     * constructor
     * @memberof com_yung_util_Toolbar
     * @param {string} containerDivId - toolbar container id
     * @param {string} btnCSS - button css style name
     * @param {string} color - toolbar background color
     */
    init: function(containerDivId, btnCSS, color){
        $Class.validate(this.classProp.name, containerDivId);
        if (containerDivId == null) {
            throw 'please specify container div id';
        }
        var divEle = document.getElementById(containerDivId);
        if (divEle == null) {
            throw 'container div not exists!';
        }
        jQuery("#" + containerDivId).css("width", "100%");
        jQuery("#" + containerDivId).css("height", "24px");
        jQuery("#" + containerDivId).css("padding-top", "5px");
        jQuery("#" + containerDivId).css("padding-bottom", "5px");
        jQuery("#" + containerDivId).css("margin", "1px");
        jQuery("#" + containerDivId).css("background-color", color);
        this.container = divEle;
        if (btnCSS == null) {
            throw 'please specify btnCSS';
        }
        this.btnCSS = document.getElementById(btnCSS);
        if (this.btnCSS == null) {
            var css = document.createElement("style");
            css.type = "text/css";
            document.body.appendChild(css);
            this.btnCSS = css;
        }
        return this;
    },
    
    /** 
     * create and add separator element
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Toolbar
     * @return {HTMLElement} separator element
     */
    addSeparator : function() {
        var elt = document.createElement('span');
        jQuery(elt).css("float", "left");
        jQuery(elt).css("text-decoration", "none");
        jQuery(elt).css("margin", "0px");
        jQuery(elt).css("font-family", "Arial");
        jQuery(elt).css("font-size", "17px");
        jQuery(elt).css("height", "19px");
        jQuery(elt).css("padding-left", "5px");
        jQuery(elt).css("padding-right", "5px");
        jQuery(elt).css("color", "rgb(33, 115, 70)");
        elt.innerHTML = "&vert;";
        this.container.appendChild(elt);
        return elt;
    },
    
    /** 
     * create button by item schema and set button function
     * 
     * @instance
     * @memberof com_yung_util_Toolbar
     * @param  {Array} items - item schema array
     */
    addItems : function(items) {
        for ( var i = 0; i < items.length; i++) {
            var item = items[i];
            var itemImg = item.img;
            var itemLabel = item.label;
            var itemTooltip = item.tooltip;
            var itemFunct = item.funct;
            if (itemImg == '-' || itemLabel == '-') {
                this.addSeparator();
            } else {
            	var divId = jQuery(this.container).attr("id");
            	this.addItem("com_yung_util_Toolbar_" + divId + "_toolbtn-" + i, itemImg, itemLabel, itemTooltip);
            }
        }
        this.initFunction(items);
    },
    
    /** 
     * initialize button function
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Toolbar
     * @param {Array} items - item array
     */
    initFunction : function(items) {
        for ( var i = 0; i < items.length; i++) {
            var item = items[i];
            var itemFunct = item.funct;
            if (itemFunct != null) {
            	var divId = jQuery(this.container).attr("id");
                this.addClickHandler("com_yung_util_Toolbar_" + divId + "_toolbtn-" + i, itemFunct);
            }
        }
    },
    addClickHandler : function(id, funct) {
        if (funct != null) {
            var ele = jQuery( "#" + id )[0];
            if (ele == null) {
            	throw "ele[id='" + id + "'] not found!";
            }
            jQuery( "#" + id ).bind( "click", function() {
                funct(ele);
            });
        }
    },
    
    
    /** 
     * initialize button style, image or label
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Toolbar
     * @param  {string} id - button id
     * @param  {string} itemImg - button image path
     * @param  {string} label - button label text
     * @param  {string} tooltip - button tooltip
     * @return {HTMLElement} button element
     */
    addItem : function(id, itemImg, label, tooltip) {
        var elt = this.addButton(id, itemImg, label, tooltip);
        this.createCSS(itemImg, tooltip);
        return elt;
    },
    addButton : function(id, itemImg, label, tooltip) {
        var elt = this.createButton(itemImg, tooltip);
        this.initElement(id, elt, label, tooltip);
        this.container.appendChild(elt);
        return elt;
    },
    createButton : function(itemImg, tooltip) {
        var inner = document.createElement('div');
        var elt = document.createElement('a');
        elt.setAttribute('href', 'javascript:void(0);');
        elt.className = 'toolButton';
        inner.className = 'toolbtn-' + this.replaceAll(tooltip, ' ', '_');
        inner.setAttribute('style', 'width: 19px; height: 19px;');
        elt.appendChild(inner);
        return elt;
    },
    initElement : function(id, elt, label, tooltip) {
        elt.setAttribute('id', id);
        if (label != null && label != '') {
            elt.innerHTML = label;
            elt.setAttribute('style', 'height: 19px;');
        } else {
            elt.setAttribute('style', 'width: 20px;');
        }
        // Adds tooltip
        if (tooltip != null) {
            elt.setAttribute('title', tooltip);
        }
    },
    createCSS : function(itemImg, tooltip) {
        var html = this.btnCSS.innerHTML;
        var append = '.toolbtn-' + this.replaceAll(tooltip, ' ', '_') + '{ background-image: url(' + itemImg + '); background-size: 19px 19px; }';
        this.btnCSS.innerHTML = html + append;
    },
    createLabel : function(label) {
        var elt = document.createElement('a');
        elt.setAttribute('href', 'javascript:void(0);');
        elt.className = 'toolLabel';
        mxUtils.write(elt, label);
        return elt;
    }
    
});

com_yung_util_Toolbar.instance = function (containerDivId, btnCSS, color) {
    return $Class.getInstance("com.yung.util.Toolbar", containerDivId, btnCSS, color);
}
