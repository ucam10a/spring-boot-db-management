/**
 * Abstract HTML template
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_RawTemplate = $Class.extend({
	
	classProp : { 
		name : "com.yung.util.RawTemplate",
		unimplemented : ['setData', 'getData', 'toHtml']
	},
	
	/**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_RawTemplate
     */
	template : null,
	
	/**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     */
	init : function () {
        this.template = new com.yung.util.BasicList('string');
        return this;
    },
    
    /** 
     * add HTML to template
     * 
     * @instance
     * @memberof com_yung_util_RawTemplate
     * @param  {string} html - HTML code
     */
    add : function (html) {
        if (typeof html != 'string') {
            throw "argument type only allows string";
        }
        this.template.add(html);
    },
    
    /** 
     * search HTML code and find string pattern then return an array
     * 
     * @instance
     * @memberof com_yung_util_RawTemplate
     * @param  {string} html - source HTML code
     * @return  {Array} string pattern array
     */
    searchPattern : function (html) {
        var ret = [];
        var index = 0;
        while (html.indexOf('{{', index) != -1) {
            var startIdx = html.indexOf('{{', index);
            var endIdx = html.indexOf('}}', index);
            var pattern = html.substring(startIdx + 2, endIdx);
            ret.push(pattern);
            index = endIdx + 2;
        }
        return ret;
    },
    
    /** 
     * remove string pattern in HTML code
     * 
     * @instance
     * @memberof com_yung_util_RawTemplate
     * @param  {string} html - source HTML code
     * @return  {string} html without string pattern
     */
    removePattern : function (html) {
    	var result = html;
    	var patternArray = this.searchPattern(html);
    	for (var i = 0; i < patternArray.length; i++) {
    		result = this.replaceAll(result, "{{" + patternArray[i] + "}}", '');
    	}
    	return result;
    }
});

/**
 * simple HTML template, only one line.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SimpleTemplate = com_yung_util_RawTemplate.extend({
    
	classProp : { 
		name : "com.yung.util.SimpleTemplate" 
	},
    
	/**
     * data object to render HTML
     * @member {object}
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     */
	data : null,
    
	/**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     */
	template : null,
	
	/**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     * @param  {string} tmpStr - one line template string
     * @param  {string} key - data key
     * @param  {string} value - data value
     */
	init : function (tmpStr, key, value) {
		var data = {};
		data[key] = value;
        this.setData(data);
        this.template = new com.yung.util.BasicList('string');
        this.template.add(tmpStr);
        return this;
    },
    
    /** 
     * set data object to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     * @param  {object} data - data object
     */
    setData : function (data) {
        if (data == null) {
            this.data = {};
        } else {
            if (typeof data != 'object') {
                throw "data is not object";
            } else {
                this.data = data;
            }
        }
    },
    
    /** 
     * get data object
     * 
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     * @return  {object} data object
     */
    getData : function () {
    	return this.data;
    },
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     * @param  {object} data - data object optional
     * @return  {string} html replaced by render key/value
     */
    toHtml : function (data) {
        if (data != null) {
            this.setData(data);
        }
        var html = '';
        for (var i = 0; i < this.template.size(); i++) {
            html = html + this.template.get(i);
        }
        var keyArray = this.searchPattern(html);
        for (var i = 0; i < keyArray.length; i++) {
        	var key = keyArray[i];
        	var value = this.data[key];
        	if (typeof value == 'string' || typeof value == 'number') {
                html = this.replaceAll(html, "{{" + key + "}}", value + '');
            }
        }
        return html;
    }
});

/**
 * Basic HTML template
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_BasicTemplate = com_yung_util_RawTemplate.extend({
    
	classProp : { 
		name : "com.yung.util.BasicTemplate" 
	},
    
	/**
     * data object to render HTML
     * @member {object}
     * @instance
     * @memberof com_yung_util_BasicTemplate
     */
	data : null,
    
	/**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_BasicTemplate
     */
	template : null,
	
	/**
     * function map for render HTML
     * @member {com_yung_util_Map}
     * @instance
     * @memberof com_yung_util_BasicTemplate
     */
	conditionMap : null,
    
	/**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     * @param  {object} data - data object to render HTML
     */
	init : function (data) {
        this.setData(data);
        this.template = new com.yung.util.BasicList('string');
        this.conditionMap = new com.yung.util.Map('string', 'function');
        return this;
    },
    
    /** 
     * set data object to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @param  {object} data - data object
     */
    setData : function (data) {
        if (data == null) {
            this.data = {};
        } else {
            if (typeof data != 'object') {
                throw "data is not object";
            } else {
                this.data = data;
            }
        }
    },
    
    /** 
     * get data array object
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @return  {object} data object
     */
    getData : function () {
        return this.data;
    },
    
    /**
     * Callback to override data value for render HTML
     *
     * @callback overrideRenderValue
     * @param {object} data - data object
     * @param {string} key - render key
     * @param {string} originValue - original render value
     */
    /** 
     * set data object to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @param  {string} key - render key
     * @param  {overrideRenderValue} condition - a callback to run
     */
    setCondition : function (key, condition) {
        if (condition == null) {
            return;
        } else {
            if (typeof condition == 'function') {
                this.conditionMap.put(key, condition);
            } else {
                throw "condition is not function";
            }
        }
    },
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @param  {object} data - data object optional
     * @return  {string} html replaced by render key/value
     */
    toHtml : function (data) {
        if (data != null) {
            this.setData(data);
        }
        var html = '';
        for (var i = 0; i < this.template.size(); i++) {
            html = html + this.template.get(i);
        }
        var keyArray = this.searchPattern(html);
        for (var i = 0; i < keyArray.length; i++) {
        	var key = keyArray[i];
        	var value = this.data[key];
        	if (this.conditionMap.get(key) != null) {
                var customVal = this.conditionMap.get(key)(data, key, value);
                value = customVal;
            }
        	if (typeof value == 'string' || typeof value == 'number') {
                html = this.replaceAll(html, "{{" + key + "}}", value + '');
            }
        }
        return html;
    }
});

/**
 * Array HTML template, to render basic template multiple times 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ArrayTemplate = com_yung_util_RawTemplate.extend({
    
	classProp : { 
		name : "com.yung.util.ArrayTemplate" 
	},
	
	/**
     * basic template to render HTML
     * @member {com_yung_util_BasicTemplate}
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     */
    basicTmp : null,
    
    /**
     * data array to render HTML
     * @member {Array}
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     */
    dataArray : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     * @param  {Array} dataArray - data array to render HTML
     * @param  {com_yung_util_BasicTemplate} basicTmp - basic template to render HTML
     */
    init : function (dataArray, basicTmp) {
        this.setDataArray(dataArray);
        this.setBasicTmp(basicTmp);
        this.template = new com.yung.util.BasicList('string');
        return this;
    },
    
    /** 
     * set data array to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @param  {Array} dataArray - data array
     */
    setDataArray : function (dataArray) {
        if (dataArray == null) {
            this.dataArray = [];
        } else {
            if (jQuery.isArray(dataArray) == false) {
                throw "dataArray is not array";
            } else {
                this.dataArray = dataArray;
            }
        }
    },
    setData : function (dataArray) {
        this.setDataArray(dataArray);
    },
    
    /** 
     * get data array object
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @return  {object} data object
     */
    getDataArray : function () {
        return this.dataArray;
    },
    getData : function () {
        return this.dataArray;
    },
    
    /** 
     * set basic template to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @param  {com_yung_util_BasicTemplate} basicTmp - basic template
     */
    setBasicTmp : function (basicTmp) {
        if (basicTmp == null) {
            this.basicTmp = null;
        } else {
            var valid = basicTmp instanceof com_yung_util_BasicTemplate;
            if (valid == false) {
                throw "basicTmp is not instance of com.yung.util.BasicTemplate";
            } else {
                this.basicTmp = basicTmp;
            }
        }
    },
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @param  {Array} dataArray - data array optional
     * @return  {string} html replaced by render key/value
     */
    toHtml : function (dataArray) {
        if (dataArray != null) {
            this.setDataArray(dataArray);
        }
        var html = "";
        if (this.basicTmp == null && this.template.size() > 0) {
        	// use template to create basicTmp
        	this.basicTmp = new com.yung.util.BasicTemplate();
        	for (var i = 0; i < this.template.size(); i++) {
        		this.basicTmp.add(this.template.get(i));
        	}
        }
        if (this.basicTmp != null) {
            for (var i = 0; i < this.dataArray.length; i++) {
                this.basicTmp.setData(this.dataArray[i]);
                html = html + this.basicTmp.toHtml();
            }
        }
        return html;
    }
});

/**
 * Complex template data
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TemplateData = $Class.extend({
    
	classProp : { 
		name : "com.yung.util.TemplateData"
	},
    
	/**
     * basic template map
     * @member {com_yung_util_Map}
     * @instance
     * @memberof com_yung_util_TemplateData
     */
    tmpMap : null,
    
    /**
     * data map
     * @member {com_yung_util_Map}
     * @instance
     * @memberof com_yung_util_TemplateData
     */
    dataMap : null,
    
	/**
	 * constructor
     * @memberof com_yung_util_TemplateData
     */
	init : function() {
		this.tmpMap = new com_yung_util_Map('string', 'object');
		this.dataMap = new com_yung_util_Map('string', 'object');
    },
    
    /** 
     * add template and data
     * 
     * @instance
     * @memberof com_yung_util_TemplateData
     * @param  {string} key - pattern key [template key] 
     * @param  {com_yung_util_RawTemplate} template - template
     * @param  {object | Array | com_yung_util_TemplateData} data - data object
     */
    addTemplate : function (key, template, data) {
    	if (key == null) {
            return;
        } else {
        	if (template == null) {
        		return ;
        	} else if (template instanceof com_yung_util_ArrayTemplate) {
            	if (data == null) {
                	return;
                } else if (jQuery.isArray(data) == true) {
                	this.tmpMap.put(key, template);
                	this.dataMap.put(key, data);
                } else {
                	throw "data is not Array";
                }
            } else if (template instanceof com_yung_util_ComplexTemplate) {
            	if (data == null) {
                	return;
                } else if (data instanceof com_yung_util_TemplateData) {
                	this.tmpMap.put(key, template);
                	this.dataMap.put(key, data);
                } else {
                	throw "data is not com.yung.util.TemplateData";
                }
            } else if (template instanceof com_yung_util_RawTemplate) {
                if (data == null) {
                    return;
                } else if (typeof data == 'object') {
                    this.tmpMap.put(key, template);
                    this.dataMap.put(key, data);
                } else {
                    throw "data is not object";
                }
            } else {
                throw "template is not instance of com.yung.util.RawTemplate";
                
            }
        }
    },
    
    /** 
     * get basic template map to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_TemplateData
     * @return  {com_yung_util_Map} basic template map
     */
    getTmpMap : function () {
    	return this.tmpMap;
    },
    
    /** 
     * get data map to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_TemplateData
     * @return {com_yung_util_Map} data map
     */
    getDataMap : function () {
    	return this.dataMap;
    }
});    


/**
 * Complex HTML template, to render two types of template
 * basic template and array template
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ComplexTemplate = com_yung_util_RawTemplate.extend({
	
    classProp : { 
    	name : "com.yung.util.ComplexTemplate" 
    },
    
    /**
     * complex template data
     * @member {com_yung_util_TemplateData}
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     */
    data : null,
    
    /**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     */
    template : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_ComplexTemplate
     * @param  {com_yung_util_Map} tmpMap - basic template map
     * @param  {com_yung_util_Map} dataMap - data map
     */
    init : function () {
        this.data = new com_yung_util_TemplateData();
        this.template = new com.yung.util.BasicList('string');
        return this;
    },
    
    /** 
     * add template and data
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @param  {string} key - pattern key [template key] 
     * @param  {com_yung_util_RawTemplate} template - template
     * @param  {object | Array | com_yung_util_TemplateData} data - data object
     */
    addTemplate : function (key, template, data) {
        this.data.addTemplate(key, template, data);
    },
    
    /** 
     * set complex template data to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @param  {com_yung_util_Map} dataMap - data map
     */
    setData : function (data) {
        if (data == null) {
            this.data = new com_yung_util_TemplateData();
        } else {
        	var valid = _validType(com_yung_util_TemplateData, data);
        	if (valid == false) {
        		throw "data type is not com.yung.util.TemplateData";
        	}
            this.data = data;
        }
    },
    
    /** 
     * get data object
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @return  {object} data object
     */
    getData : function () {
        return this.data;
    },
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @param  {com_yung_util_TemplateData} data - complex tempalte data [optional]
     * @return  {string} html replaced by render key/value
     */
    toHtml : function (data) {
        if (data != null) {
            this.setData(data);
        }
        var html = '';
        for (var i = 0; i < this.template.size(); i++) {
            html = html + this.template.get(i);
        }
        var tmpMap = this.data.getTmpMap();
        var dataMap = this.data.getDataMap();
        var keyArray = tmpMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var key = keyArray[i];
            var tmp = tmpMap.get(key);
            var dataObj = dataMap.get(key);
            html = this.replaceAll(html, "{{" + key + "}}", tmp.toHtml(dataObj) + '');
        }
        return html;
    }
});

jQuery( document ).ready(function() {
    if (typeof com_yung_util_Collection == 'undefined') {
        alert("yung-BasicTemplate.js requires yung-Collection.js!");
    }
    if (typeof com_yung_util_Map == 'undefined') {
        alert("yung-BasicTemplate.js requires yung-Map.js!");
    }
});