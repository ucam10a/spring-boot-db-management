/**
 * An easy tool to get position of element
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Position = $Class.extend({
	
    classProp : { 
    	name : "com.yung.util.Position" 
    },
    
    /**
     * HTML element to get position
     * @member {HTMLElement}
     * @instance
     * @memberof com_yung_util_Position
     */
    element : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_Position
     * @param  {HTMLElement} element - HTML element to get position
     */
    init : function (element) {
        if (element == null) {
            throw "element can not be null";
        }
        if (typeof element.get == 'function') {
            if (element[0] == null) {
                throw "element can not be null";
            }
            this.element = element;
        } else {
            this.element = jQuery(element);
        }
        return this;
    },
    
    /** 
     * check current window if in iframe
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {boolean} in iframe or not
     */
    inIframe : function () {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    },
    
    
    
    getParentBaseTop : function () {
        var inIframe = this.inIframe();
        if (inIframe == true) {
            var doc = window.parent.document.documentElement;
            var top = (window.parent.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
            if (top == 0) {
                // quirk mode will get 0, so check if browser is IE quirk
                var browser = com_yung_util_getbrowser();
                if (browser == 'msie') {
                    // IE quirk
                    top = window.parent.document.body.scrollTop - (doc.clientTop || 0);
                }
            }
            return top;
        } else {
            return 0;
        }
    },
    getParentBaseLeft : function () {
        var inIframe = this.inIframe();
        if (inIframe == true) {
            var doc = window.parent.document.documentElement;
            var left = (window.parent.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
            if (left == 0) {
                // quirk mode will get 0, so check if browser is IE quirk
                var browser = com_yung_util_getbrowser();
                if (browser == 'msie') {
                    // IE quirk
                    left = window.parent.document.body.scrollLeft - (doc.clientLeft || 0);
                }
            }
            return left;
        } else {
            return 0;
        }
    },
    
    
    
    
    /** 
     * get document element top position, use by scroll bar occurs
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {number} document element top position
     */
    getBaseTop : function () {
    	var doc = document.documentElement;
    	var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    	if (top == 0) {
    	    // quirk mode will get 0, so check if browser is IE quirk
    	    var browser = com_yung_util_getbrowser();
    	    if (browser == 'msie') {
    	        // IE quirk
                top = document.body.scrollTop - (doc.clientTop || 0);
            }
    	}
    	return top;
    },
    
    /** 
     * get document element left position, use by scroll bar occurs
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {number} document element left position
     */
    getBaseLeft : function () {
    	var doc = document.documentElement;
    	var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
    	if (left == 0) {
            // quirk mode will get 0, so check if browser is IE quirk
    	    var browser = com_yung_util_getbrowser();
            if (browser == 'msie') {
                // IE quirk
                left = document.body.scrollLeft - (doc.clientLeft || 0);
            }
        }
    	return left;
    },
    
    /** 
     * get specified element bottom left position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {HTMLElement} base - base HTMLElement [optional]
     * @return  {object} specified element bottom left position
     */
    getBotLeftPosition : function (offsetX, offsetY, base) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseEle = null;
        if (base != null) {
            if (typeof base.get == 'function') {
                baseEle = base;
            } else {
                baseEle = jQuery(base);
            }
        }
        var scrollTop = 0;
        var scrollLeft = 0;
        if (base == null) {
            scrollTop = this.getBaseTop();
            scrollLeft = this.getBaseLeft();
        } else {
            scrollTop = baseEle.scrollTop();
            scrollLeft = baseEle.scrollLeft();
        }
        var rect = this.getBoundingClientRect(baseEle);
        var pos = {};
        pos.left = rect.left + scrollLeft + offsetX;
        pos.top = rect.top + scrollTop + this.element.height() + offsetY;
        return pos;
    },
    
    /** 
     * get specified element top left position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {HTMLElement} base - base HTMLElement [optional]
     * @return  {object} specified element top left position
     */
    getTopLeftPosition : function (offsetX, offsetY, base) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseEle = null;
        if (base != null) {
            if (typeof base.get == 'function') {
                baseEle = base;
            } else {
                baseEle = jQuery(base);
            }
        }
        var scrollTop = 0;
        var scrollLeft = 0;
        if (base == null) {
            scrollTop = this.getBaseTop();
            scrollLeft = this.getBaseLeft();
        } else {
            scrollTop = baseEle.scrollTop();
            scrollLeft = baseEle.scrollLeft();
        }
        var rect = this.getBoundingClientRect(baseEle);
        var pos = {};
        pos.left = rect.left + scrollLeft + offsetX;
        pos.top = rect.top + scrollTop + offsetY;
        return pos;
    },
    
    /** 
     * get specified element top right position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {HTMLElement} base - base HTMLElement [optional]
     * @return  {object} specified element top right position
     */
    getTopRightPosition : function (offsetX, offsetY, base) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseEle = null;
        if (base != null) {
            if (typeof base.get == 'function') {
                baseEle = base;
            } else {
                baseEle = jQuery(base);
            }
        }
        var scrollTop = 0;
        var scrollLeft = 0;
        if (base == null) {
            scrollTop = this.getBaseTop();
            scrollLeft = this.getBaseLeft();
        } else {
            scrollTop = baseEle.scrollTop();
            scrollLeft = baseEle.scrollLeft();
        }
        var rect = this.getBoundingClientRect(baseEle);
        var pos = {};
        pos.left = rect.left + scrollLeft + this.element.width() + offsetX;
        pos.top = rect.top + scrollTop + offsetY;
        return pos;
    },
    
    /** 
     * get specified element bottom right position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {HTMLElement} base - base HTMLElement [optional]
     * @return  {object} specified element bottom right position
     */
    getBotRightPosition : function (offsetX, offsetY, base) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseEle = null;
        if (base != null) {
            if (typeof base.get == 'function') {
                baseEle = base;
            } else {
                baseEle = jQuery(base);
            }
        }
        var scrollTop = 0;
        var scrollLeft = 0;
        if (base == null) {
            scrollTop = this.getBaseTop();
            scrollLeft = this.getBaseLeft();
        } else {
            scrollTop = baseEle.scrollTop();
            scrollLeft = baseEle.scrollLeft();
        }
        var rect = this.getBoundingClientRect(baseEle);
        var pos = {};
        pos.left = rect.left + scrollLeft + this.element.width() + offsetX;
        pos.top = rect.top + scrollTop + this.element.height() + offsetY;
        return pos;
    },
    
    /** 
     * get center position of specified width and height
     * 
     * @memberof com_yung_util_Position
     * @param  {number} width - width [default 0]
     * @param  {number} height - height [default 0]
     * @return  {object} specified element bottom left position
     */
    getCenterPosition : function (width, height) {
        if (width == null) {
            width = 0;
        }
        if (height == null) {
            height = 0;
        }
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        if (windowHeight > jQuery(document).height()) {
            windowHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth > jQuery(document).width()) {
            windowWidth = jQuery(document).width();
        }
        var pos = {};
        pos["left"] = this.getBaseLeft() + (windowWidth - width) / 2;
        pos["top"] = this.getBaseTop() + (windowHeight - height) / 2;
        if (pos["left"] < 0 || pos["top"] < 0) {
            var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
            var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        	return this.getCenterPositionWithWinSize(width, height, windowWidth, windowHeight);
        }
        if (pos.top < 0) {
            pos.top = 0;
        }
        if (pos.left < 0) {
            pos.left = 0;
        }
        return pos;
    },
    
    /** 
     * get center position of specified width, height, windowWidth and windowHeight
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} width - width [default 0]
     * @param  {number} height - height [default 0]
     * @param  {number} windowWidth - window width
     * @param  {number} windowHeight - window height
     * @return  {object} specified element bottom left position
     */
    getCenterPositionWithWinSize : function (width, height, windowWidth, windowHeight) {
        if (width == null) {
            width = 0;
        }
        if (height == null) {
            height = 0;
        }
        if (windowWidth == null) {
        	throw "windowWidth not defined";
        }
        if (windowHeight == null) {
            throw "windowHeight not defined";
        }
        var pos = {};
        pos["left"] = this.getBaseLeft() + (windowWidth - width) / 2;
        pos["top"] = this.getBaseTop() + (windowHeight - height) / 2;
        if (pos.top < 0) {
            pos.top = 0;
        }
        if (pos.left < 0) {
            pos.left = 0;
        }
        return pos;
    },
    
    /** 
     * get bounding client rect
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {HTMLElement} base - base element [optional]
     * @return  {object} bounding client rect
     */
    getBoundingClientRect : function (base) {
        var ret = {};
        var rect = this.element.get(0).getBoundingClientRect();
        if (base != null) {
            var baseEle = null;
            if (typeof base.get == 'function') {
                baseEle = base;
            } else {
                baseEle = jQuery(base);
            }
            var baseRect = baseEle.get(0).getBoundingClientRect();
            ret.top = rect.top - baseRect.top;
            ret.left = rect.left - baseRect.left;
        } else {
            ret.top = rect.top;
            ret.left = rect.left;
        }
        return ret;
    },
    
    /** 
     * get window screen size
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {object} specified element width and height
     */
    getScrennSize : function () {
        var ret = {};
        ret["height"] = jQuery(window).height();
        ret["width"] = jQuery(window).width();
        return ret;
    },
    
    /** 
     * get window screen center position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} width - width [default 0]
     * @param  {number} height - height [default 0]
     * @return  {object} specified element bottom left position
     */
    getScreenCenterPosition : function (width, height) {
        if (width == null) {
            width = 0;
        }
        if (height == null) {
            height = 0;
        }
        var winSize = this.getScrennSize();
        var pos = {};
        pos["left"] = (winSize.width - width) / 2;
        pos["top"] = (winSize.height - height) / 2;
        if (pos.top < 0) {
            pos.top = 0;
        }
        if (pos.left < 0) {
            pos.left = 0;
        }
        return pos;
    }
    
});

/** 
 * Returns browser scroll bar width
 * 
 * @memberof com_yung_util_Position
 * @return {number} scroll bar width
 */
com_yung_util_Position.getScrollbarWidth = function () {
    var ret = yung_global_var["com_yung_util_Position.getScrollbarWidth.result"];
    if (ret != null) {
        return ret;
    }
    // Creating invisible container
    var outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.overflow = 'scroll'; // forcing scrollbar to appear
    outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps
    document.body.appendChild(outer);

    // Creating inner element and placing it in the container
    var inner = document.createElement('div');
    outer.appendChild(inner);

    // Calculating difference between container's full width and the child width
    var scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);
    var scrollbarHeight = (outer.offsetHeight - inner.offsetHeight);
    if (scrollbarWidth >= scrollbarHeight && scrollbarWidth > 2 && scrollbarWidth < 25) {
        scrollbarHeight = scrollbarWidth;
    } else if (scrollbarHeight >= scrollbarWidth && scrollbarHeight > 2 && scrollbarHeight < 25) {
        scrollbarWidth = scrollbarHeight;
    } else if (scrollbarHeight > 10 && scrollbarHeight < 25) {
        scrollbarWidth = scrollbarHeight;
    } else if (scrollbarWidth > 10 && scrollbarWidth < 25) {
        scrollbarHeight = scrollbarWidth;
    } else if (scrollbarWidth < 2 && scrollbarHeight < 2) {
        scrollbarWidth = 17;
        scrollbarHeight = 17;
    } else {
        scrollbarWidth = 17;
        scrollbarHeight = 17;
    }
    // Removing temporary elements from the DOM
    outer.parentNode.removeChild(outer);
    yung_global_var["com_yung_util_Position.getScrollbarWidth.result"] = scrollbarWidth;
    yung_global_var["com_yung_util_Position.getScrollbarHeight.result"] = scrollbarHeight;
    return scrollbarWidth;
}


/** 
 * Returns browser scroll bar height
 * 
 * @memberof com_yung_util_Position
 * @return {number} scroll bar height
 */
com_yung_util_Position.getScrollbarHeight = function () {
    var ret = yung_global_var["com_yung_util_Position.getScrollbarHeight.result"];
    if (ret != null) {
        return ret;
    }
    // Creating invisible container
    var outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.overflow = 'scroll'; // forcing scrollbar to appear
    outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps
    document.body.appendChild(outer);

    // Creating inner element and placing it in the container
    var inner = document.createElement('div');
    outer.appendChild(inner);

    // Calculating difference between container's full width and the child width
    var scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);
    var scrollbarHeight = (outer.offsetHeight - inner.offsetHeight);
    if (scrollbarWidth >= scrollbarHeight && scrollbarWidth > 2 && scrollbarWidth < 25) {
        scrollbarHeight = scrollbarWidth;
    } else if (scrollbarHeight >= scrollbarWidth && scrollbarHeight > 2 && scrollbarHeight < 25) {
        scrollbarWidth = scrollbarHeight;
    } else if (scrollbarHeight > 10 && scrollbarHeight < 25) {
        scrollbarWidth = scrollbarHeight;
    } else if (scrollbarWidth > 10 && scrollbarWidth < 25) {
        scrollbarHeight = scrollbarWidth;
    } else if (scrollbarWidth < 2 && scrollbarHeight < 2) {
        scrollbarWidth = 17;
        scrollbarHeight = 17;
    } else {
        scrollbarWidth = 17;
        scrollbarHeight = 17;
    }
    // Removing temporary elements from the DOM
    outer.parentNode.removeChild(outer);
    yung_global_var["com_yung_util_Position.getScrollbarWidth.result"] = scrollbarWidth;
    yung_global_var["com_yung_util_Position.getScrollbarHeight.result"] = scrollbarHeight;
    return scrollbarHeight;
}