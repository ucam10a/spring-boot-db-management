/**
 * Convenient validate tool to check string
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Validator = {
    
    classProp : { 
        name : "com.yung.util.Validator"
    },
    
    /** 
     * to check input string if it is valid email
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    email : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(input)) {  
            return true;
        }  
        return false;
    },
    
    /** 
     * to check input string if it is number
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    digit : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^\d+$/.test(input)) {
            return true;
        }
        return false;
    },
    
    /** 
     * to check input string if it is an account type
     * account type only allow digit[0-9] and letter[A-Z, a-z],
     * not allow special symbol, like '@','$'
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    account : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^[A-Za-z0-9_.]+$/.test(input)) {
            return true;
        }
        return false;
    },
    
    /** 
     * to check input string if it is letter[A-Z, a-z]
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    letter : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^[A-Za-z_.]+$/.test(input)) {
            return true;
        }
        return false;
    },
    
    /** 
     * to check input string if it is letter[A-Z, a-z]
     * or a special symbol in an array
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @param {string} specialChars - special character array
     * @return {boolean} valid or not
     */
    letterExtchar : function (input, specialChars) {
        if (input == null || input == '') {
            return true;
        }
        if (this.ascii(input) == false) {
            return false;
        }
        for (var i = 0; i < input.length; i++) {
            var c = input.charAt(i) + "";
            if (this.letter(c) == false) {
                var valid = false;
                for (var j = 0; j < specialChars.length; j++) {
                    if (c == specialChars[j]) {
                        valid = true;
                        break;
                    }
                }
                if (valid == false) {
                    return false;
                }
            }
        }
        return true;
    },
    
    /** 
     * to check input string if it is ascii code
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    ascii : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^[\x00-\x7F]*$/.test(input)) {
            return true;
        }
        return false;
    },
    
    /** 
     * to check input string if it is account type[0-9, A-Z, a-z]
     * or a special symbol in an array
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @param {string} specialChars - special character array
     * @return {boolean} valid or not
     */
    accountExtchar : function (input, specialChars) {
        if (input == null || input == '') {
            return true;
        }
        if (this.ascii(input) == false) {
            return false;
        }
        for (var i = 0; i < input.length; i++) {
            var c = input.charAt(i) + "";
            if (this.account(c) == false) {
                var valid = false;
                for (var j = 0; j < specialChars.length; j++) {
                    if (c == specialChars[j]) {
                        valid = true;
                        break;
                    }
                }
                if (valid == false) {
                    return false;
                }
            }
        }
        return true;
    }
};
$Y.reg(com_yung_util_Validator);