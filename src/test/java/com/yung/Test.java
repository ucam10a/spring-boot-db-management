package com.yung;

import com.yung.tool.SecretTool;

public class Test {
    
	public static void main(String[] args) throws InterruptedException {
        
	    for (int i = 0; i < 100; i++) {
	        String secret = SecretTool.createSecret("Test");
	        System.out.println(secret);
	        System.out.println(SecretTool.validateSecret(secret, "Test", 1));
	    }
	    
    }

}